class HomeController < ApplicationController
  require 'open-uri'

  def index
    @url = params[:search]
  	if @url
  		@arr = Array.new
      begin
        doc = Nokogiri::HTML(open(@url))
        doc.css(".new-cars-results-box").each do |item|
          hash = Hash.new
   
          type = item.at_css(".new-car-name").text
          link = "http://uae.yallamotor.com"+item.at_css(".new-car-name")[:href]
          nested_doc = Nokogiri::HTML(open(link))
          spec_price = nested_doc.css(".prices-and-specs") 
          spec_price.css(".new-cars-results").each do |car_detail|
            hash["type"] = type
            detail = car_detail.css(".new-cars-results-text")
            hash["title"] = detail.at('h4 a').text
            hash["price"] = detail.at('h4 span').text

            @arr << hash
          end
        end
      rescue
      end
  		
  	end
  end
end

